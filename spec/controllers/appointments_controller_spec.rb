require 'rails_helper'

RSpec.describe AppointmentsController, type: :controller do

  def authenticated_user(request, user)
    token = JWT.encode({user_id: user.id}, 'z3creT')
    headers = { 'Authorization': "Bearer #{token}" }
    request.headers.merge! headers
  end

  describe 'GET #new' do
    it 'Will return 200 for authorized user' do
      user = User.create({email: 'wahyu@wahyu.com', password: 'asdasd', name: 'Si wahyu', age: 29})
      authenticated_user(request, user)

      get :new

      expect(response.status).to eq(200)
    end
    it 'Will return 401 for unauthorized user' do
      get :new
      expect(response.status).to eq(401)
    end
  end


  def create_doctor_schedule(start_time, end_time)
    hospital = Hospital.create(name: 'Rumah Sakit Indonesia', address: 'Jakarta Pusat')
    doctor = Doctor.create(name: 'Dr. John Doe, Sp.PD', hospital_id: hospital.id, specialty: 'Dokter Spesialis Penyakit Dalam')
    doctor_schedule = DoctorSchedule.create(doctor_id: doctor.id, start_time: start_time, end_time: end_time)
  end

  describe 'POST #create' do
    context 'Book an appointment' do
      it 'Will return 201 for book an appointment > 30 minutes before schedule start' do
        doctor_schedule = create_doctor_schedule((Time.now + 40.minutes), (Time.now + 2.hours))
        user  = User.create({email: 'wahyu@wahyu.com', password: 'asdasd', name: 'Si wahyu', age: 29})
        authenticated_user(request, user)

        post :create, params: { appointment: {doctor_schedule_id: doctor_schedule.id, user_id: user.id} }

        expect(response.status).to eq(201)
      end
      it "Will return 500 for maximum 10 booking per doctor's schedule" do
        doctor_schedule = create_doctor_schedule((Time.now + 2.hours), (Time.now + 5.hours))
        (1..10).each do |n|
          other_users = User.create({email: "wahyu_#{n}@wahyu.com", password: "asdasd", name: "Si wahyu_#{n}", age: 29})
          other_appointments = Appointment.create({doctor_schedule_id: doctor_schedule.id, user_id: other_users.id})
        end

        # Create 11th appointment
        user  = User.create({email: 'wahyu@wahyu.com', password: 'asdasd', name: 'Si wahyu', age: 29})
        authenticated_user(request, user)

        post :create, params: { appointment: {doctor_schedule_id: doctor_schedule.id, user_id: user.id} }

        expect(response.status).to eq(500)
      end
    end
  end

end
