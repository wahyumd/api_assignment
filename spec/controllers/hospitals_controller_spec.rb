require 'rails_helper'

RSpec.describe HospitalsController, type: :controller do

  describe 'GET #index' do
    it 'Will return 200' do
      get :index
      expect(response.status).to eq(200)
    end
  end

end
