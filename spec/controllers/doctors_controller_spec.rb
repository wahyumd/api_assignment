require 'rails_helper'

RSpec.describe DoctorsController, type: :controller do

  describe 'GET #index' do
    it 'Will return 200' do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET #appointments' do
    it 'will routes to doctors/:slug/appointments' do
      expect(:get => "/doctors/dr-john-doe-sp-pd/appointments").to route_to(
        :controller => "doctors",
        :action => "appointments",
        :slug => "dr-john-doe-sp-pd"
      )
    end
  end

end
