require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe 'POST #create' do
    context 'With valid attributes' do
      it 'returns 201' do
        post :create, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29} }
        expect(response.status).to eq(201)
      end
    end
    context 'With invalid attributes' do
      it 'returns 422 for null email' do
        post :create, params: { user: {name: 'Si wahyu', password: 'asdasd', age: 29} }
        expect(response.status).to eq(422)
      end
      it 'returns 422 null password' do
        post :create, params: { user: {email: 'wahyu@wahyu.com', age: 29} }
        expect(response.status).to eq(422)
      end
      it 'returns 422 for uniqueness of email' do
        post :create, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29} }
        post :create, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'qweqwe', age: 29} }
        expect(response.status).to eq(422)
      end
    end
  end

  context 'POST #login' do
    it 'allow user to sign in' do
      post :create, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29} }
      post :login, params: { user: {email: 'wahyu@wahyu.com', password: 'asdasd' } }
      expect(response.status).to eq(200)
    end
    it 'returns 401 for invalid username' do
      post :create, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29} }
      post :login, params: { user: {email: 'wahyu@mega.com', name: 'Si wahyu', password: 'asdasd'} }
      expect(response.status).to eq(401)
    end
    it 'returns 401 for invalid password' do
      post :create, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29} }
      post :login, params: { user: {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'qweqwe'} }
      expect(response.status).to eq(401)
    end
  end

end
