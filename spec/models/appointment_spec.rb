require 'rails_helper'

RSpec.describe Appointment, type: :model do
  def create_doctor_schedule(start_time, end_time)
    hospital = Hospital.create(name: 'Rumah Sakit Indonesia', address: 'Jakarta Pusat')
    doctor = Doctor.create(name: 'Dr. John Doe, Sp.PD', hospital_id: hospital.id, specialty: 'Dokter Spesialis Penyakit Dalam')
    doctor_schedule = DoctorSchedule.create(doctor_id: doctor.id, start_time: start_time, end_time: end_time)
  end

  describe 'Create an appointment' do
    it 'will generate queue' do
      doctor_schedule = create_doctor_schedule((Time.now + 40.minutes), (Time.now + 2.hours))
      user  = User.create({email: 'wahyu@wahyu.com', password: 'asdasd', name: 'Si wahyu', age: 29})

      appointment = Appointment.new({doctor_schedule_id: doctor_schedule.id, user_id: user.id}).save
      queue = Appointment.last.queue
      expect(queue).to eq(1)
    end
    context 'validation test' do
      it 'will failed to create an appointment < 30 minutes before schedule start' do
        doctor_schedule = create_doctor_schedule((Time.now + 20.minutes), (Time.now + 2.hours))
        user  = User.create({email: 'wahyu@wahyu.com', password: 'asdasd', name: 'Si wahyu', age: 29})

        appointment = Appointment.new({doctor_schedule_id: doctor_schedule.id, user_id: user.id}).save
        expect(appointment).to eq(false)
      end

      it "will failed to create 11th appointment in a doctor's schedule" do
        doctor_schedule = create_doctor_schedule((Time.now + 2.hours), (Time.now + 5.hours))
        (1..10).each do |n|
          other_users = User.create({email: "wahyu_#{n}@wahyu.com", password: "asdasd", name: "Si wahyu_#{n}", age: 29})
          other_appointments = Appointment.create({doctor_schedule_id: doctor_schedule.id, user_id: other_users.id})
        end

        # Create 11th appointment
        user  = User.create({email: 'wahyu@wahyu.com', password: 'asdasd', name: 'Si wahyu', age: 29})

        appointment = Appointment.new({doctor_schedule_id: doctor_schedule.id, user_id: user.id}).save
        expect(appointment).to eq(false)
      end
    end
  end

  context 'Scope Tests' do
    it "shows doctor's appointment" do
      doctor_schedule = create_doctor_schedule((Time.now + 2.hours), (Time.now + 5.hours))
      (1..10).each do |n|
        users = User.create({email: "wahyu_#{n}@wahyu.com", password: "asdasd", name: "Si wahyu_#{n}", age: 29})
        appointments = Appointment.create({doctor_schedule_id: doctor_schedule.id, user_id: users.id})
      end

      doctor = Doctor.first
      doctor_appointments = Appointment.doctor_appointments(doctor.id)
      expect(1).to eq(1)
    end
  end
end
