require 'rails_helper'

RSpec.describe User, type: :model do
  context 'POST #create' do
    let (:params) { {email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29} }
    it 'creates a new user' do
      user = User.new(params).save
      expect(user).to eq(true)
    end
    it 'validate presence of email' do
      user = User.new({name: 'Si wahyu', password: 'asdasd', age: 29}).save
      expect(user).to eq(false)
    end
    it 'validate presence of password' do
      user = User.new({email: 'wahyu@wahyu.com', name: 'Si wahyu', age: 29}).save
      expect(user).to eq(false)
    end
    it 'validate uniqueness of email' do
      user_1 = User.new({email: 'wahyu@wahyu.com', name: 'Si wahyu', password: 'asdasd', age: 29}).save
      user_2 = User.new({email: 'wahyu@wahyu.com', name: 'Si wahyu 2', password: 'qweqwe', age: 29}).save
      expect(user_2).to eq(false)
    end
  end
end
