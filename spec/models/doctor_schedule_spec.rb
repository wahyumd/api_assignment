require 'rails_helper'

RSpec.describe DoctorSchedule, type: :model do
  describe "Create Doctor's Schedules" do
    it "should save succsessfully" do
      hospital = Hospital.create(name: 'Rumah Sakit Indonesia', address: 'Jakarta Pusat')
      doctor = Doctor.create(name: 'Dr. John Doe, Sp.PD', hospital_id: hospital.id, specialty: 'Dokter Spesialis Penyakit Dalam')
      doctor_schedule = DoctorSchedule.new({doctor_id: doctor.id, start_time: '2020-08-20 08:00:00', end_time: '2020-08-20 12:00:00'}).save

      expect(doctor_schedule).to eq(true)
    end
  end
end
