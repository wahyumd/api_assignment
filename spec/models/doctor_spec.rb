require 'rails_helper'

RSpec.describe Doctor, type: :model do
  describe 'Create Slug Test' do
    it 'ensure slug is created' do
      hospital = Hospital.create(name: 'Rumah Sakit Indonesia', address: 'Jakarta Pusat')
      doctor = Doctor.create(name: 'Dr. John Doe, Sp.PD', hospital_id: hospital.id, specialty: 'Dokter Spesialis Penyakit Dalam')

      expect(doctor.slug).to eq('dr-john-doe-sp-pd')
    end
  end
end
