require 'rails_helper'

RSpec.describe Hospital, type: :model do
  describe 'Create Slug Test' do
    it 'ensure slug is created' do
      hospital = Hospital.create(name: 'Rumah Sakit Indonesia', address: 'Jakarta Pusat')
      expect(hospital.slug).to eq('rumah-sakit-indonesia')
    end
  end
end
