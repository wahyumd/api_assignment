class Doctor < ApplicationRecord
  belongs_to :hospital
  has_many :doctor_schedules

  before_create :slugify

  def slugify
    self.slug = name.parameterize
  end

end
