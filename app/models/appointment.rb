class Appointment < ApplicationRecord
  belongs_to :doctor_schedule
  belongs_to :user

  scope :doctor_appointments, -> (doctor_id) { joins(:doctor_schedule).where('doctor_schedules.doctor_id = ?', doctor_id) }

  validate :maximum_30_minutes_before_the_schedule_start
  validate :maximum_10_appointment_per_schedule

  before_create :set_queue

  def set_queue
    counter = Appointment.where(doctor_schedule_id: self.doctor_schedule_id).count
    self.queue = counter + 1
  end

  def maximum_30_minutes_before_the_schedule_start
    schedule_start = self.doctor_schedule.start_time
    booking_time = Time.zone.now

    if schedule_start.to_date == booking_time.to_date
      time_difference = (schedule_start - booking_time) / 30.minutes
      if time_difference < 1
        errors.add(:created_at, 'The maximum booking within 30 minutes before the doctor starts the schedule')
      end
    end
  end

  def maximum_10_appointment_per_schedule
    total_schedules = Appointment.where(doctor_schedule_id: self.doctor_schedule.id).count
    if total_schedules >= 10
      errors.add(:doctor_schedule_id, "The Doctor's schedule is full")
    end
  end

end
