class AppointmentSerializer < ActiveModel::Serializer
  attributes :id, :doctor_schedule_id, :user_id, :queue, :doctor

  belongs_to :doctor_schedule
  belongs_to :user

  def doctor
    DoctorSerializer.new(object.doctor_schedule.doctor).as_json
  end
end
