class DoctorSerializer < ActiveModel::Serializer
  attributes :id, :name, :specialty, :slug
  belongs_to :hospital
  has_many :doctor_schedules
end
