class HospitalSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :slug
end
