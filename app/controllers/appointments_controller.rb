class AppointmentsController < ApplicationController
  before_action :authorized, only: [:new, :create]

  def new
    @appointment = Appointment.new
    render json: {data: @appointment}, status: :ok
  end

  def create
    @appointment = Appointment.new(appointment_params)
    if @appointment.save
      render json: @appointment, status: :created
    else
      render json: {message: @appointment.errors.messages}, status: :internal_server_error
    end
  end

  private

  def appointment_params
    params.require(:appointment).permit(:doctor_schedule_id, :user_id, :queue)
  end
end
