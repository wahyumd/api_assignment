class DoctorsController < ApplicationController
  before_action :set_doctor, only: [:show, :appointments]

  def index
    @doctors = Doctor.all
    render json: @doctors, status: :ok
  end

  def show
    render json: @doctor, status: :ok
  end

  def appointments
    @appointments = Appointment.doctor_appointments(@doctor.id)
    render json: @appointments, status: :ok
  end

  private

  def set_doctor
    @doctor = Doctor.find_by_slug(params[:slug]) rescue nil
  end


end
