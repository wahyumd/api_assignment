class HospitalsController < ApplicationController

  def index
    @hospitals = Hospital.all
    render json: @hospitals, status: :ok
  end

end
