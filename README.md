# Backend Developer Assignment

1. Create an API for login and register by using:
1.1 email **[done]**
1.2 social media account
2. Create an API list to display all doctors and hospitals **[done]**

3. Create an API where user can book a schedule with doctor **[done]**

4. The maximum user can book within 30 minutes before the doctor starts the schedule. **[done]**

5. A total of 10 users can book the same doctor. **[done]**

6. Create an API to display doctor schedule with patients **[done]**

7. Write unit test with Rspec **[done]**

####Ruby version :
```
ruby 2.6.5
```

####Configuration:
Create .env file for database credentials
```
DB_USERNAME: username
DB_PASSWORD: password
DB_HOST: host
DB_PORT: 5432
```

####Running apps
```
bundle install

rails db:create db:migrate db:seed

rails s
```

#### Run the test
```
rake spec
```