Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resource :users, only: [:create]
  post '/login', to: 'users#login'

  resources :hospitals, only: [:index]
  resources :doctors, only: [:index, :show], param: :slug do
    member do
      get 'appointments'
    end
  end
  resources :appointments, only: [:new, :create]
end
