users = User.create([
  {email: "wahyu1@wahyu.com", name: "wahyu1", password: "asdasd", age: 29},
  {email: "wahyu2@wahyu.com", name: "wahyu2", password: "asdasd", age: 39},
  {email: "wahyu3@wahyu.com", name: "wahyu3", password: "asdasd", age: 49},
  {email: "wahyu4@wahyu.com", name: "wahyu4", password: "asdasd", age: 59},
  {email: "wahyu5@wahyu.com", name: "wahyu5", password: "asdasd", age: 69},
  {email: "wahyu6@wahyu.com", name: "wahyu6", password: "asdasd", age: 79},
  {email: "wahyu7@wahyu.com", name: "wahyu7", password: "asdasd", age: 89},
  {email: "wahyu8@wahyu.com", name: "wahyu8", password: "asdasd", age: 99},
  {email: "wahyu9@wahyu.com", name: "wahyu9", password: "asdasd", age: 19},
  {email: "wahyu10@wahyu.com", name: "wahyu10", password: "asdasd", age: 29}
])

hospitals = Hospital.create([
  {name: 'RS Zahirah', address: 'Jagakarsa, Jakarta Selatan'},
  {name: 'RS Tiara', address: 'Karawaci, Tangerang'}
])

doctors = Doctor.create([
  {
    name: 'Prof. dr. Zarkasih Anwar, Sp.A(K)',
    specialty: 'Dokter Spesialis Anak',
    hospital: hospitals.first
  },
  {
    name: 'Prof. dr. Zubairi Djoerban, Sp.PD-KHOM',
    specialty: 'Dokter Spesialis Penyakit Dalam',
    hospital: hospitals.last
  }
])

doctor_schedules = DoctorSchedule.create([
  {
    doctor: doctors.first,
    start_time: '2020-08-23 18:00:00',
    end_time: '2020-08-23 22:00:00'
  },
  {
    doctor: doctors.first,
    start_time: '2020-08-25 18:00:00',
    end_time: '2020-08-25 22:00:00'
  },
  {
    doctor: doctors.first,
    start_time: '2020-08-27 18:00:00',
    end_time: '2020-08-27 22:00:00'
  },
  {
    doctor: doctors.last,
    start_time: '2020-08-24 07:00:00',
    end_time: '2020-08-24 11:00:00'
  },
  {
    doctor: doctors.last,
    start_time: '2020-08-26 07:00:00',
    end_time: '2020-08-26 11:00:00'
  },
  {
    doctor: doctors.last,
    start_time: '2020-08-28 07:00:00',
    end_time: '2020-08-28 11:00:00'
  }
])

users.each do |user|
  Appointment.create([
    {
      doctor_schedule: doctor_schedules.first,
      user_id: user.id
    }
  ])
end