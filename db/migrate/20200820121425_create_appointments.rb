class CreateAppointments < ActiveRecord::Migration[6.0]
  def change
    create_table :appointments do |t|
      t.references :doctor_schedule, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :queue

      t.timestamps
    end
  end
end
