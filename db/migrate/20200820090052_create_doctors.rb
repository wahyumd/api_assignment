class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :name
      t.references :hospital, null: false, foreign_key: true
      t.string :specialty
      t.string :slug

      t.timestamps
    end
  end
end
